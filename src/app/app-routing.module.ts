import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/auth/login',
    pathMatch: 'full'
  },
  {
    path: 'auth',
    loadChildren: () => import('./modules/auth/auth.module').then(m => m.AuthModule),
  },
  {
    path: 'main',
    loadChildren: () => import('./modules/main/main.module').then(m => m.MainPageModule),
  },
  {
    path: 'boarding',
    loadChildren: () => import('./modules/boarding/boarding.module').then( m => m.BoardingPageModule)
  },
  {
    path: 'complaints',
    loadChildren: () => import('./modules/complaints/complaints.module').then( m => m.ComplaintsPageModule)
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, useHash: true  })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
