import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
export class Table {
  FAVOURITE = 'favourite';
  RECENT = 'recent';
  USER = 'user';
  API_KEY = 'api_key';
  PHOTO = 'photo';
  DATAPERIBADI = 'dataperibadi';
  
  protected obs = {};
  protected mut = {}

  constructor(){
    for(let table in this){
      if(table != "obs" && table != "mut"){
          this.obs[this[table].toString()] = new BehaviorSubject(null);
          this.mut[this[table].toString()] = "";
      }
    }
  }
}