import { Injectable } from '@angular/core';
import { Table } from './table';
import { Storage } from '@ionic/storage';


@Injectable({
  providedIn: 'root'
})
export class StorageService extends Table {

  constructor(
    private storage: Storage
  ) {
    super();
  }

  set(table, value){
    try{
      this.mut[table] = value;
      this.obs[table].next(value);
      return this.storage.set(table, value);
    } catch (e) {
      throw e;
    }
  }

  get<T>(table){
    try{
      return this.storage.get(table);
    } catch (e) {
      throw e;
    }
  }

  observable(table){
    try{
      return this.obs[table];
    } catch (e) {
      throw e;
    }
  }

  value(table){
    try{
      return this.mut[table];
    } catch (e) {
      throw e;
    }
  }

  clear(table){
    try{
      this.mut[table] = null;
      this.obs[table].next(null);
      this.storage.remove(table);
    } catch (e) {
      throw e;
    }
  }
}
