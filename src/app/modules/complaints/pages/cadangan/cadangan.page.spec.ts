import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CadanganPage } from './cadangan.page';

describe('CadanganPage', () => {
  let component: CadanganPage;
  let fixture: ComponentFixture<CadanganPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadanganPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CadanganPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
