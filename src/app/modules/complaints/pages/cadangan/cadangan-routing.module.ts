import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CadanganPage } from './cadangan.page';

const routes: Routes = [
  {
    path: '',
    component: CadanganPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CadanganPageRoutingModule {}
