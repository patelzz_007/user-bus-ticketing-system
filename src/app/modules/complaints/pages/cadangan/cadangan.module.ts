import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CadanganPageRoutingModule } from './cadangan-routing.module';

import { CadanganPage } from './cadangan.page';
import { AppComponentsModule } from 'src/app/components/components.module';
import { AppDirectivesModule } from 'src/app/directives/directives.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CadanganPageRoutingModule,
    AppComponentsModule,
    AppDirectivesModule
  ],
  declarations: [CadanganPage]
})
export class CadanganPageModule {}
