import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SemakanAduanPage } from './semakan-aduan.page';

const routes: Routes = [
  {
    path: '',
    component: SemakanAduanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SemakanAduanPageRoutingModule {}
