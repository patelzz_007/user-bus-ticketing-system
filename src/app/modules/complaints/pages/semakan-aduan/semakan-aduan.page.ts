import { Component, OnInit } from '@angular/core';
import { SemakanaduanUI, AduandetailUI } from 'src/app/models/ui/aduan';

@Component({
  selector: 'app-semakan-aduan',
  templateUrl: './semakan-aduan.page.html',
  styleUrls: ['./semakan-aduan.page.scss'],
})
export class SemakanAduanPage implements OnInit {

  semakan: Array<SemakanaduanUI> = [];
  start2: Array<SemakanaduanUI> = [];
  aduan: Array<AduandetailUI> =[];
  start: Array<AduandetailUI> =[];


  constructor() { }

  ngOnInit() {
  }

  async setSemakan(status) {
    // this.semakan = await this.semakanApi.semakanaduan(status);
    this.semakan.forEach((value, index) => {
      this.semakan[index].isShow = false;
    })

    this.start2 = JSON.parse(JSON.stringify(this.semakan));
    this.start2.forEach((value, index) => {
      this.start2[index].isShow = false;
    });

  }

  async getAduan(ticketlog) {
    // const ticket = await this.semakan.map(t => t.ticketnum).toString();
    // this.aduan = await this.semakanApi.aduandetail(ticketlog);
      this.aduan.forEach((value, index) => {
      this.aduan[index].isShow = false;
      })
    
    this.start = JSON.parse(JSON.stringify(this.aduan));
    this.start.forEach((value, index) => {
      this.start[index].isShow = false;
    });

  }

  showHideButton(item) {
    item.isShow = !item.isShow;
  }

  showHideText(item) {
    return item.isShow ? '↑' : '↓';
  }

  filter(event) {
    const searchTerm = event.detail.value;

    this.semakan = this.start2.filter((item: SemakanaduanUI) => {
      return (
        item.ticketnum.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1 ||
        item.definasiuser.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1 ||
        item.dtreceived.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1 ||
        item.definasiuser.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
      );
    });

    this.aduan = this.start.filter((item: AduandetailUI) => {
      return (
        item.actiontaken.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1 ||
        item.probdesc.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1 
      );
    });
  }


}
