import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SemakanAduanPage } from './semakan-aduan.page';

describe('SemakanAduanPage', () => {
  let component: SemakanAduanPage;
  let fixture: ComponentFixture<SemakanAduanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SemakanAduanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SemakanAduanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
