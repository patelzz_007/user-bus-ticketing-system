import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SemakanAduanPageRoutingModule } from './semakan-aduan-routing.module';

import { SemakanAduanPage } from './semakan-aduan.page';
import { AppComponentsModule } from 'src/app/components/components.module';
import { AppDirectivesModule } from 'src/app/directives/directives.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SemakanAduanPageRoutingModule,
    AppComponentsModule,
    AppDirectivesModule
  ],
  declarations: [SemakanAduanPage]
})
export class SemakanAduanPageModule {}
