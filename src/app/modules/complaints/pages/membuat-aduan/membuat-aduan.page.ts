import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Plugins, CameraResultType, CameraSource, Geolocation, Capacitor } from '@capacitor/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { Platform } from '@ionic/angular';

import { WidgetService } from 'src/app/services/common';

const { Camera } = Plugins;




@Component({
  selector: 'app-membuat-aduan',
  templateUrl: './membuat-aduan.page.html',
  styleUrls: ['./membuat-aduan.page.scss'],
})
export class MembuatAduanPage implements OnInit {
  @ViewChild('filePicker', { static: false }) filePickerRef: ElementRef<HTMLInputElement>;
  latitude: number;
  longitude: number;
  isDesktop: boolean;
  photo: SafeResourceUrl;
  uploadfilename: any;
  file: string;


  constructor(

    private widgets: WidgetService,
    private sanitizer: DomSanitizer,
    private platform: Platform,

  ) { }

  ngOnInit() {
  }

  success(loading) {
    loading.dismiss();
    this.widgets.showSuccessModal();
  }

  async takePicture(type: string) {
    if (!Capacitor.isPluginAvailable('Camera') || (this.isDesktop && type === 'gallery')) {
      this.filePickerRef.nativeElement.click();
      return;
    }

    const image = await Camera.getPhoto({
      quality: 100,
      width: 400,
      allowEditing: false,
      resultType: CameraResultType.DataUrl,
      source: CameraSource.Prompt
    });

    this.photo = this.sanitizer.bypassSecurityTrustResourceUrl(image && (image.dataUrl))
    this.file = image.dataUrl;
    // this.form.uploadfilename = this.file;
    // console.log(this.form.uploadfilename);
  }

  onFileChoose(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];
    const pattern = /image-*/;
    const reader = new FileReader();

    if (!file.type.match(pattern)) {
      console.log('File format not supported');
      return;
    }

    reader.onload = () => {
      this.photo = reader.result.toString();
    };
    reader.readAsDataURL(file);
    // console.log(reader);
  }


  
  async getLocation() {
    const loading = await this.widgets.loading();
    loading.present(); 
    const coordinates = await Geolocation.getCurrentPosition();
    this.latitude = coordinates.coords.latitude;
    this.longitude = coordinates.coords.longitude;
    this.success(loading);

    // console.log('current', coordinates);

    // const apiz = await this.aduanApi.adress(this.latitude, this.longitude);
    // console.log(apiz);
  }


}
