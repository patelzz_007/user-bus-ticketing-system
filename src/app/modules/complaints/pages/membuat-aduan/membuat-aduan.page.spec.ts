import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MembuatAduanPage } from './membuat-aduan.page';

describe('MembuatAduanPage', () => {
  let component: MembuatAduanPage;
  let fixture: ComponentFixture<MembuatAduanPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MembuatAduanPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MembuatAduanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
