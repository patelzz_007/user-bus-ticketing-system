import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MembuatAduanPage } from './membuat-aduan.page';

const routes: Routes = [
  {
    path: '',
    component: MembuatAduanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MembuatAduanPageRoutingModule {}
