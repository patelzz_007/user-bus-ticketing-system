import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MembuatAduanPageRoutingModule } from './membuat-aduan-routing.module';

import { MembuatAduanPage } from './membuat-aduan.page';

import { AppComponentsModule } from 'src/app/components/components.module';
import { AppDirectivesModule } from 'src/app/directives/directives.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MembuatAduanPageRoutingModule,
    AppDirectivesModule,
    AppComponentsModule
  ],
  declarations: [MembuatAduanPage]
})
export class MembuatAduanPageModule {}
