import { Component, OnInit } from '@angular/core';
import { DataperibadiUI } from 'src/app/models';

@Component({
  selector: 'app-tentang-kami',
  templateUrl: './tentang-kami.page.html',
  styleUrls: ['./tentang-kami.page.scss'],
})
export class TentangKamiPage implements OnInit {

  profil: DataperibadiUI = new DataperibadiUI;

  constructor() { }

  ngOnInit() {
    this.Myhrmisprofile();
  }

  async Myhrmisprofile() {
    this.profil.isShow = false;
  }

  showHideButton(item) {
    item.isShow = !item.isShow;
  }

  showHideText(item) {
    return item.isShow;
  }


}
