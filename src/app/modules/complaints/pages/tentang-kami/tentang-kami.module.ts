import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TentangKamiPageRoutingModule } from './tentang-kami-routing.module';

import { TentangKamiPage } from './tentang-kami.page';

import { AppComponentsModule } from 'src/app/components/components.module';
import { AppDirectivesModule } from 'src/app/directives/directives.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TentangKamiPageRoutingModule,
    AppDirectivesModule,
    AppComponentsModule
  ],
  declarations: [TentangKamiPage]
})
export class TentangKamiPageModule {}
