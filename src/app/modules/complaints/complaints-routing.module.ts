import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ComplaintsPage } from './complaints.page';

const routes: Routes = [
  {
    path: '',
    component: ComplaintsPage,
    children: [
      {
        path: 'membuat-aduan',
        loadChildren: () => import('./pages/membuat-aduan/membuat-aduan.module').then( m => m.MembuatAduanPageModule)
      },
      {
        path: 'semakan-aduan',
        loadChildren: () => import('./pages/semakan-aduan/semakan-aduan.module').then( m => m.SemakanAduanPageModule)
      },
      {
        path: 'cadangan',
        loadChildren: () => import('./pages/cadangan/cadangan.module').then( m => m.CadanganPageModule)
      },
      {
        path: 'tentang-kami',
        loadChildren: () => import('./pages/tentang-kami/tentang-kami.module').then( m => m.TentangKamiPageModule)
      },

    ]
  },

  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ComplaintsPageRoutingModule {}
