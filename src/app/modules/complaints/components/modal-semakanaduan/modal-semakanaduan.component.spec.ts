import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ModalSemakanaduan } from './modal-semakanaduan.component';

describe('ModalSemakanaduan', () => {
  let component: ModalSemakanaduan;
  let fixture: ComponentFixture<ModalSemakanaduan>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalSemakanaduan ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ModalSemakanaduan);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
