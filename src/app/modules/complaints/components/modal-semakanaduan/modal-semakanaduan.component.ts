import { Component, OnInit } from '@angular/core';
import { AduandetailUI } from 'src/app/models/ui/aduan';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal-semakanaduan',
  templateUrl: './modal-semakanaduan.component.html',
  styleUrls: ['./modal-semakanaduan.component.scss'],
})
export class ModalSemakanaduan implements OnInit {
  aduan: Array<AduandetailUI> = [];

  constructor(
    private modalCtrl: ModalController
  ) { }

  ngOnInit() { }

  dismiss() {
    this.modalCtrl.dismiss();
  }

}
