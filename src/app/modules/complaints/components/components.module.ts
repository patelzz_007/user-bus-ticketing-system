import { FormsModule } from '@angular/forms';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { ModalSemakanaduan } from './modal-semakanaduan/modal-semakanaduan.component';
import { AppComponentsModule } from 'src/app/components/components.module';
import { AppDirectivesModule } from 'src/app/directives/directives.module';

const components = [
    ModalSemakanaduan,
]
@NgModule({
    declarations: components,
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        AppComponentsModule,
        AppDirectivesModule,
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ],
    exports: components,
})
export class MainComponentsModule {}
