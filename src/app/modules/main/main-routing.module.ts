import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MainPage } from './main.page';

export const routes: Routes = [
  {
    path: '',
    component: MainPage,
    children: [

      {
        path: 'home',
        loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
      },
      {
        path: 'main-profil',
        loadChildren: () => import('./pages/profil/profil.module').then( m => m.ProfilPageModule)
      },
      {
        path: 'shortlink',
        loadChildren: () => import('./pages/shortlink/shortlink.module').then( m => m.ShortlinkPageModule)
      },
      {
        path: 'purchase-tickets',
        loadChildren: () => import('./pages/purchase-tickets/purchase-tickets.module').then( m => m.PurchaseTicketsPageModule)
      },
      {
        path: 'view-my-profile',
        loadChildren: () => import('./pages/view-my-profile/view-my-profile.module').then( m => m.ViewMyProfilePageModule)
      },
      {
        path: 'transaction-history',
        loadChildren: () => import('./pages/transaction-history/transaction-history.module').then( m => m.TransactionHistoryPageModule)
      },     
    ]
  },


  

  



];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MainPageRoutingModule {}
