import { Component, OnInit, ViewChild } from '@angular/core';

import { MenuController, IonSlides } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';

import { AppCommon } from 'src/app/services/common';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {


  slideOptsOne = {
    initialSlide: 0,
    slidesPerView: 1,
    autoplay: true
  };

  favs: [] = [];
  recents = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private menuCtrl: MenuController,
    private app: AppCommon,
    ) {

    }

    ionViewWillEnter() {
      this.setFav();
      this.setRecent();
    }
  
  
  ngOnInit() {

  }


  async setFav() {
    this.favs = await this.app.getFavourite();
  }

  async setRecent() {
    this.recents = await this.app.getRecent();
    
  }
}
