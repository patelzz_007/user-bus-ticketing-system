import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular'

@Component({
  selector: 'app-transaction-history',
  templateUrl: './transaction-history.page.html',
  styleUrls: ['./transaction-history.page.scss'],
})
export class TransactionHistoryPage implements OnInit {

  constructor(
    private nav: NavController,
  ) { }

  ngOnInit() {
  }

  goToBoarding(){
    this.nav.navigateForward('/boarding/check-in')
  }

  boardTheBus(){
    this.nav.navigateForward('/boarding/board-the-bus')
  }

  enRoute(){
    this.nav.navigateForward('/boarding/en-route')
  }

}


