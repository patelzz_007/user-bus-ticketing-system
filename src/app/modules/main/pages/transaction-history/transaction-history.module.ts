import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TransactionHistoryPageRoutingModule } from './transaction-history-routing.module';

import { TransactionHistoryPage } from './transaction-history.page';
import { AppComponentsModule } from 'src/app/components/components.module';
import { AppDirectivesModule } from 'src/app/directives/directives.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TransactionHistoryPageRoutingModule,
    AppComponentsModule,
    AppDirectivesModule
  ],
  declarations: [TransactionHistoryPage]
})
export class TransactionHistoryPageModule {}
