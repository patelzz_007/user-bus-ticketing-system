import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ShortlinkPage } from './shortlink.page';

describe('ShortlinkPage', () => {
  let component: ShortlinkPage;
  let fixture: ComponentFixture<ShortlinkPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShortlinkPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ShortlinkPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
