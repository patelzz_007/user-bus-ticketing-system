import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ShortlinkPage } from './shortlink.page';

const routes: Routes = [
  {
    path: '',
    component: ShortlinkPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ShortlinkPageRoutingModule {}
