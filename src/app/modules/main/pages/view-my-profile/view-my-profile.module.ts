import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewMyProfilePageRoutingModule } from './view-my-profile-routing.module';

import { ViewMyProfilePage } from './view-my-profile.page';

import { AppComponentsModule } from 'src/app/components/components.module';
import { AppDirectivesModule } from 'src/app/directives/directives.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewMyProfilePageRoutingModule,
    AppComponentsModule,
    AppDirectivesModule
  ],
  declarations: [ViewMyProfilePage]
})
export class ViewMyProfilePageModule {}
