import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PurchaseTicketsPageRoutingModule } from './purchase-tickets-routing.module';

import { PurchaseTicketsPage } from './purchase-tickets.page';
import { AppComponentsModule } from 'src/app/components/components.module';
import { AppDirectivesModule } from 'src/app/directives/directives.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PurchaseTicketsPageRoutingModule,
    AppComponentsModule,
    AppDirectivesModule
  ],
  declarations: [PurchaseTicketsPage]
})
export class PurchaseTicketsPageModule {}
