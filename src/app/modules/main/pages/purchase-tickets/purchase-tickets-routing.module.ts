import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PurchaseTicketsPage } from './purchase-tickets.page';

const routes: Routes = [
  {
    path: '',
    component: PurchaseTicketsPage,
    children:[
      {
        path: '',
        loadChildren: () => import('./pages/main/main.module').then( m => m.PurchaseTicketMainPageModule)
      },
      {
        path: 'choose-seat',
        loadChildren: () => import('./pages/choose-seat/choose-seat.module').then( m => m.ChooseSeatPageModule)
      },
      
      {
        path: 'payment',
        loadChildren: () => import('./pages/payment/payment.module').then( m => m.PaymentPageModule)
      }, 
    ]
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PurchaseTicketsPageRoutingModule {}
