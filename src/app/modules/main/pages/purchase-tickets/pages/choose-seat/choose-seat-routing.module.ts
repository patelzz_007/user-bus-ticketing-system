import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChooseSeatPage } from './choose-seat.page';

const routes: Routes = [
  {
    path: '',
    component: ChooseSeatPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChooseSeatPageRoutingModule {}
