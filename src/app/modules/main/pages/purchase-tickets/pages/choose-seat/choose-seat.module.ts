import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChooseSeatPageRoutingModule } from './choose-seat-routing.module';

import { ChooseSeatPage } from './choose-seat.page';
import { AppComponentsModule } from 'src/app/components/components.module';
import { AppDirectivesModule } from 'src/app/directives/directives.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChooseSeatPageRoutingModule,
    AppComponentsModule,
    AppDirectivesModule
    
  ],
  declarations: [ChooseSeatPage]
})
export class ChooseSeatPageModule {}
