import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular'

@Component({
  selector: 'app-choose-seat',
  templateUrl: './choose-seat.page.html',
  styleUrls: ['./choose-seat.page.scss'],
})
export class ChooseSeatPage implements OnInit {

  private currentNumber = 0;

  constructor(
    private nav : NavController
  ) { }

  ngOnInit() {
    console.log("ChooseSeatPage");
  }

  checkOut(){
    this.nav.navigateForward('/main/purchase-tickets/payment')
  }

  incrementQty() {
    this.currentNumber++;
  }

  decrementQty() {
    this.currentNumber--;
  }

}
