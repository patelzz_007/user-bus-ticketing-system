import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ChooseSeatPage } from './choose-seat.page';

describe('ChooseSeatPage', () => {
  let component: ChooseSeatPage;
  let fixture: ComponentFixture<ChooseSeatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseSeatPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ChooseSeatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
