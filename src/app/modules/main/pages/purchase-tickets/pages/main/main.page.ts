import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-purchase-tickets-main',
  templateUrl: './main.page.html',
  styleUrls: ['./main.page.scss'],
})
export class PurchaseTicketMainPage implements OnInit {

  selectedRadioItem:any;
  
  constructor(
    private nav : NavController
  ) { }

  ngOnInit() {
    console.log("PurchaseTicket")
  }

  radioSelect(event) {
    console.log("radioSelect",event.detail);
    this.selectedRadioItem = event.detail;
  }

  chooseSeat(){
    this.nav.navigateForward('/main/purchase-tickets/choose-seat');
  }
}
