import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PurchaseTicketMainPageRoutingModule } from './main-routing.module';

import { PurchaseTicketMainPage } from './main.page';
import { AppComponentsModule } from 'src/app/components/components.module';
import { AppDirectivesModule } from 'src/app/directives/directives.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PurchaseTicketMainPageRoutingModule,
    AppComponentsModule,
    AppDirectivesModule
  ],
  declarations: [PurchaseTicketMainPage]
})
export class PurchaseTicketMainPageModule {}
