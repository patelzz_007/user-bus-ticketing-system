import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PurchaseTicketMainPage } from './main.page';

const routes: Routes = [
  {
    path: '',
    component: PurchaseTicketMainPage
  },
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PurchaseTicketMainPageRoutingModule {}
