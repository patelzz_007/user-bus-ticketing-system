import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { FunctionService } from 'src/app/services/function.service';
import { AlertController, MenuController } from '@ionic/angular';
import swal from 'sweetalert2';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.page.html',
  styleUrls: ['./payment.page.scss'],
})
export class PaymentPage implements OnInit {

  addNewPayment = false;
  private currentNumber = 0;

  constructor(
    private menuCtrl: MenuController, 
    private fun: FunctionService, 
    private dataService: DataService, 
    private alertController: AlertController
  ) { }

  ngOnInit() {
  }

  private increment () {
    this.currentNumber++;
  }
  
  private decrement () {
    this.currentNumber--;
  }

  
  addPayment(){
    this.addNewPayment = !this.addNewPayment;
  }

  done(){
    swal.fire("Awesome", "You just bought 2 awesome dresses", "success");
    this.fun.navigate('/main/home',false);
  }



  async back() {
    const alert = await this.alertController.create({
      header: 'Are you sure?',
      message: 'Do you want to cancel entering your payment info?',
      buttons: [
        {
          text: 'Yes',
          cssClass: 'mycolor',
          handler: (blah) => {
            this.fun.back();
          }
        }, {
          text: 'No',
          role: 'cancel',
          cssClass: 'mycolor',
          handler: () => {}
        }
      ]
    });

    await alert.present();
  }

}
