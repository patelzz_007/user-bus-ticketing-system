import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BoardingPage } from './boarding.page';

const routes: Routes = [
  {
    path: '',
    component: BoardingPage,
    children: [
      {
        path: 'check-in',
        loadChildren: () => import('./pages/check-in/check-in.module').then( m => m.CheckInPageModule)
      },
      {
        path: 'board-the-bus',
        loadChildren: () => import('./pages/board-the-bus/board-the-bus.module').then( m => m.BoardTheBusPageModule)
      },
      {
        path: 'en-route',
        loadChildren: () => import('./pages/en-route/en-route.module').then( m => m.EnRoutePageModule)
      },
    ]
  },


  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BoardingPageRoutingModule {}
