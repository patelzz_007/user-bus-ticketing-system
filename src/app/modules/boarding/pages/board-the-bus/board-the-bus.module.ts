import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BoardTheBusPageRoutingModule } from './board-the-bus-routing.module';

import { BoardTheBusPage } from './board-the-bus.page';

import { AppComponentsModule } from 'src/app/components/components.module';
import { AppDirectivesModule } from 'src/app/directives/directives.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BoardTheBusPageRoutingModule,
    AppComponentsModule,
    AppDirectivesModule
  ],
  declarations: [BoardTheBusPage]
})
export class BoardTheBusPageModule {}
