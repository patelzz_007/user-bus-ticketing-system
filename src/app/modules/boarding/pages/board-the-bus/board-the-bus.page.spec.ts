import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BoardTheBusPage } from './board-the-bus.page';

describe('BoardTheBusPage', () => {
  let component: BoardTheBusPage;
  let fixture: ComponentFixture<BoardTheBusPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoardTheBusPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BoardTheBusPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
