import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BoardTheBusPage } from './board-the-bus.page';

const routes: Routes = [
  {
    path: '',
    component: BoardTheBusPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BoardTheBusPageRoutingModule {}
