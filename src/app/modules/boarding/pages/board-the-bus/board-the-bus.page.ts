import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
import { FunctionService } from 'src/app/services/function.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-board-the-bus',
  templateUrl: './board-the-bus.page.html',
  styleUrls: ['./board-the-bus.page.scss'],
})
export class BoardTheBusPage implements OnInit {

  constructor(
    
    private fun: FunctionService,
    private alertController: AlertController
  ) { }

  ngOnInit() {
  }

  ok(){
    swal.fire("You just successfully boarded the bus.", "Enjoy your trip.", "success");
    this.fun.navigate('/main/transaction-history',false);
  }
}
