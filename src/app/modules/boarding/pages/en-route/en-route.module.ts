import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EnRoutePageRoutingModule } from './en-route-routing.module';

import { EnRoutePage } from './en-route.page';
import { AppComponentsModule } from 'src/app/components/components.module';
import { AppDirectivesModule } from 'src/app/directives/directives.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EnRoutePageRoutingModule,
    AppComponentsModule,
    AppDirectivesModule,
  ],
  declarations: [EnRoutePage]
})
export class EnRoutePageModule {}
