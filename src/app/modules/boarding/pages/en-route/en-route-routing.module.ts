import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EnRoutePage } from './en-route.page';

const routes: Routes = [
  {
    path: '',
    component: EnRoutePage,
    children: [
      {
        path: '',
        loadChildren: () => import('./pages/main/main.module').then( m => m.MainPageModule)
      },
      {
        path: 'trip-info',
        loadChildren: () => import('./pages/trip-info/trip-info.module').then( m => m.TripInfoPageModule)
      },
    ]
  },
  
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EnRoutePageRoutingModule {}
