import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EnRoutePage } from './en-route.page';

describe('EnRoutePage', () => {
  let component: EnRoutePage;
  let fixture: ComponentFixture<EnRoutePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnRoutePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EnRoutePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
