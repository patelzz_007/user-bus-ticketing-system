import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TripInfoPageRoutingModule } from './trip-info-routing.module';

import { TripInfoPage } from './trip-info.page';

import { AppComponentsModule } from 'src/app/components/components.module';
import { AppDirectivesModule } from 'src/app/directives/directives.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TripInfoPageRoutingModule,
    AppDirectivesModule,
    AppComponentsModule
  ],
  declarations: [TripInfoPage]
})
export class TripInfoPageModule {}
