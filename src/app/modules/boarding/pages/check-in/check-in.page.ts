import { Component, OnInit } from '@angular/core';
import swal from 'sweetalert2';
import { FunctionService } from 'src/app/services/function.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-check-in',
  templateUrl: './check-in.page.html',
  styleUrls: ['./check-in.page.scss'],
})
export class CheckInPage implements OnInit {

  constructor(
    
    private fun: FunctionService,
    private alertController: AlertController
  ) { }

  ngOnInit() {
  }

  doneCheckIn(){
    swal.fire("Awesome", "You just successfully checked in", "success");
    this.fun.navigate('/main/transaction-history',false);
  }

}
