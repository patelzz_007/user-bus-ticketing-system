import { Semakanaduan, Aduandetail } from '../base/aduan';

export class SemakanaduanUI extends Semakanaduan{
    public isShow?: boolean;
}
export class AduandetailUI extends Aduandetail{
    public isShow?: boolean;
}
