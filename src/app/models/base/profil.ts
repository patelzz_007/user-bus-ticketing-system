export class Dataperibadi{
    id: number;
    coid: number; 
    icno: string;
    name: string;
    title: string;
    birth_date: string;
    age: number;
    state_of_birth: string;
    country_of_birth: string;
    gender: string;
    nat_status: string;
    race: string;
    ethnic: string;
    bumi_status: string;
    religion: string;
    marital_status: string;
    mobile_phone: string;
    email: string;
    office_telephone: string;
    address: string;
    city: string;
    state: string;
    country: string;
    postcode: string;
}