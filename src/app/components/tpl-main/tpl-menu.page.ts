import { Component, OnInit, Input, Output, ContentChild, TemplateRef } from '@angular/core';

import { NavController } from '@ionic/angular';
import { TemplateMenuDirective } from 'src/app/directives';

@Component({
  selector: 'app-tpl-menu',
  templateUrl: './tpl-menu.page.html',
  styleUrls: ['./tpl-menu.page.scss'],
})
export class TemplateMenu implements OnInit {
  @ContentChild(TemplateMenuDirective, { static: true, read: TemplateRef }) menuTpl: TemplateRef<any>;
  @Input() title: string;
  @Input() isBack: boolean;
  

  constructor(
    private nav: NavController,
  ) { }

  ngOnInit() {
  }

  goBack() {
    this.nav.back();
  }

}
